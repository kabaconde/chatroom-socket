const socket = io.connect('http://localhost:8080');

const username = prompt('Quel est votre pseudo ?');
const room = prompt('Quelle est ta room ?');
socket.emit('new_user', {username, room});

socket.on('new_user', (params) => {
    // alert(name);
});

socket.on('new_message', (params) => {
    chat.innerHTML += `<div><em>${params.username}</em>: ${params.message}</div>`;
});

socket.on('chat_init', (CHAT) => {
    chat.innerHTML = CHAT.map(params => {
        return `<div><em>${params.username}</em>: ${params.message}</div>`
    }).join('<hr>');
});

const form = document.forms[0];
const chat = document.getElementById('chat');

form.addEventListener('submit', (e) => {
    e.preventDefault();
    if (form.elements['message'].value != '') {
        socket.emit('message', form.elements['message'].value);
    }
});